let valorA = 5;
let valorB = 4;
let valorC = 1;
let valorD = 6;
let resultado = 0;

// (5+4) //
resultado = valorA + valorB;
console.log(`El resultado de ${valorA} + ${valorB} es: ${resultado}`);

// (5+1) //
resultado = valorA + valorC;
console.log(`El resultado de ${valorA} + ${valorC} es: ${resultado}`);

// Se suma 6 al resultado anterior //
let aux = resultado;
resultado = aux + valorD;
console.log(`El resultado de ${aux} + ${valorD} es: ${resultado}`);

// (5/4) //
resultado = valorA / valorB;
console.log(`El resultado de ${valorA} dividido en ${valorB} es: ${resultado}`);

// El resto de (5/4) //
resultado = valorA % valorB;
console.log(`El resto de ${valorA} dividido en ${valorB} es: ${resultado}`);