class Estudiante {

    constructor (nombre, apellido, edad, dni, telefono) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.dni = dni;
        this.telefono = telefono;
    }

    mostrarDatos() {
        return `Nombre completo: ${this.nombre} ${this.apellido}\n
                Edad: ${this.edad}\n
                DNI: ${this.dni}\n
                Tel: ${this.telefono}`;
    }
}

const estudiante = new Estudiante('Raul', 'Alarcon', 30, 35219720, 15545352);
alert(estudiante.mostrarDatos());