const coolImages = require("cool-images");
const fs = require('fs');
var moment = require('moment');
moment().format('ll');

imagenesUrls = coolImages.many(0,0,10);

// Funcion que muestra urls por consola o maneja excepcion //
const mostrarUrls = (err, urlImg) => {
    err? console.log(err) : console.log(urlImg);
}

// Funcion que almacena las url en txt //
const guardarUrlsTxt = () => imagenesUrls.forEach((imagenUrl, idx) => {
    let urlImg = `Url ${idx}: ${imagenUrl} - ${moment()}\n`;
    fs.appendFile('./log.txt', urlImg, (err) => {
        mostrarUrls(err, urlImg);
    });
});

guardarUrlsTxt();