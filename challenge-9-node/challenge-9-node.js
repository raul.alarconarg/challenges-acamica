

let personas = [
    { nombre: 'Juan', edad: 40 },
    { nombre: 'Raul', edad: 30 },
    { nombre: 'Lia', edad: 30 },
    { nombre: 'Adrian', edad: 33 },
    { nombre: 'Martin', edad: 37 },
    { nombre: 'Franco', edad: 27 },
    { nombre: 'Maria', edad: 28 },
];

console.log(personas);