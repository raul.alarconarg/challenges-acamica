let num1;
let num2;

// Funcionamiento boton suma //
document.getElementById("btn-suma").addEventListener("click", function() {
    getValores();
    console.log("Resultado:", parseInt(num1) + parseInt(num2));
});

// Funcionamiento boton resta //
document.getElementById("btn-resta").addEventListener("click", function() {
    getValores();
    console.log("Resultado:", parseInt(num1) - parseInt(num2));
});

// Funcionamiento boton multiplicacion //
document.getElementById("btn-mult").addEventListener("click", function() {
    getValores();
    console.log("Resultado:", parseInt(num1) * parseInt(num2));
});

// Funcionamiento boton dividir //
document.getElementById("btn-div").addEventListener("click", function() {
    getValores();
    console.log("Resultado:", parseInt(num1) / parseInt(num2));
});

// Funcionamiento boton cuadrados //
document.getElementById("btn-cuad").addEventListener("click", function() {
    getValores();
    console.log("Resultado:", Math.pow(parseInt(num1), 2) + Math.pow(parseInt(num2), 2));
});

// Funcion p/ obtener valores de input //
function getValores() {
    num1 = document.getElementById("numero1").value;
    num2 = document.getElementById("numero2").value;
}