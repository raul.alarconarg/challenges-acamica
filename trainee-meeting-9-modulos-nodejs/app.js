const operaciones = require('./calculator');
const fs = require('fs');

// Funcion p/ mostrar resultado o manejar excepcion //
const ejecutarOperacion = (err, msj) => {
    err? console.log(err) : console.log (`Proceso exitoso: ${msj}`);
}

const registrarEnLog = (operacion) => fs.appendFile('./log.txt', operacion, (err) => {
    ejecutarOperacion(err, operacion);
});

// Valores a calcular //
let nro1 = 5;
let nro2 = 7;

// Ejecutando operaciones //
let result = operaciones.sumar(nro1, nro2);
registrarEnLog(`Suma: ${nro1} + ${nro2} = ${result}\n`);

result = operaciones.restar(nro1, nro2);
registrarEnLog(`Resta: ${nro1} - ${nro2} = ${result}\n`);

result = operaciones.multiplicar(nro1, nro2);
registrarEnLog(`Multiplicacion: ${nro1} * ${nro2} = ${result}\n`);

result = operaciones.dividir(nro1, nro2);
registrarEnLog(`Division: ${nro1} / ${nro2} = ${result}\n`);