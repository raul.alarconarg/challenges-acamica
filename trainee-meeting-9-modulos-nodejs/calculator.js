const sumar = (nro1, nro2) => { return nro1 + nro2; }
const restar = (nro1, nro2) => { return nro1 - nro2; }
const multiplicar = (nro1, nro2) => { return nro1 * nro2; }
const dividir = (nro1, nro2) => { return nro1 / nro2; } 

module.exports = { sumar, restar, multiplicar, dividir }