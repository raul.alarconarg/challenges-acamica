const express = require('express');
const app = express();

// Metodo que retorna array de alumnos //
const getAlumnos = (res) => res.send([
    { nombre: 'Juan', edad: 40 },
    { nombre: 'Raul', edad: 30 },
    { nombre: 'Lia', edad: 30 },
    { nombre: 'Adrian', edad: 33 },
    { nombre: 'Martin', edad: 37 },
    { nombre: 'Franco', edad: 27 },
    { nombre: 'Maria', edad: 28 },
]);    

app.get('/alumnos', (req, res) => getAlumnos(res));


app.listen(3000, function () {
    console.log('Escuchando el puerto 3000!');
  });