document.getElementById('submit').addEventListener('click', function (){
    let nombre = document.getElementById('nombre').value;
    let apellido = document.getElementById('apellido').value;
    let edad = document.getElementById('edad').value;
    let licencia = document.getElementById('licencia').value;
    let fexp = parseInt(document.getElementById('fexp').value);

    // Validando campos vacios //
    if (nombre == '' || apellido == '' || edad == '' || fexp == '') {
        alert('Todos los campos son obligatorios.');
    } else {
        // Comprobando si esta habilitado p/ conducir //
        if (edad > 18 && licencia === 'si' && fexp < 20210708) {
            alert(`${nombre} ${apellido} esta habilitado para conducir.`);
        } else {
            alert(`${nombre} ${apellido} no esta habilitado para conducir.`);
        }
    }
    
});