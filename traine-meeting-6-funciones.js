// Se pasa un numero como parametro y calcula el cuadrado del mismo //
const calcularCuadrado = numero => numero * numero

calcularCuadrado(10);

// Verifica si el usuario y contraseña se encuentra en array bidimensional de usuarios //
let datosUser = [['user1', 'pass1'], ['user2', 'pass2'], ['user3', 'pass3']];
const validarUserPass = (user, pass) => {
    let resultado = false;
    for (let i = 0; i < datosUser.length; i++) {
        if (user == datosUser[i][0] && pass == datosUser[i][1]) {
            resultado = true;
            break;
        }
    }
    return resultado;
}

validarUserPass('user1', 'pass1');

// Recibe radio por parametro y calcula su area //
function calcularArea(radio){
    return Math.PI * radio ** 2
}

// Recibe un numero por parametro y devuelve su factorial //
function obtenerFactorial(numero) {
    if (Number.isInteger(numero)) {
        let suma = 1;
        for (let i = 1; i <= numero; i++) {
            suma = suma * i;
        }
        console.log('El factorial de ' + numero +' es ' + suma);
    } else {
        console.log('No es un número entero');
        return;
    }
}